﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameSession : MonoBehaviour
{

    //[SerializeField] TextMeshProUGUI score;
    [SerializeField] int currentScore = 0;
    [SerializeField] int playerHealth = 0;



    private void Awake()
    {
        SetUpSingleton();
        //playerHealth = FindObjectOfType<Player>().health;
    }
    

    void Start()
    {       
        //score.text = currentScore.ToString();        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void SetUpSingleton()
    {
        if (FindObjectsOfType<GameSession>().Length > 1)
        {
            gameObject.SetActive(false);
            Destroy(gameObject);
        }
        else
        {
            DontDestroyOnLoad(gameObject);
        }
    }

    public void addScore(int points)
    {
        currentScore = currentScore + points;
        //score.text = currentScore.ToString();
    }
    public int GetScore()
    {
        return currentScore;
    }
    public void ResetGame()
    {
        Destroy(gameObject);
    }       

    public int GetHealth()
    {
        return playerHealth;
    }

    public void SetHealth(int val)
    {
        playerHealth = val;
    }
}
