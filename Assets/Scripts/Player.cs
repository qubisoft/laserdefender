﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{


    
    [Header("Player")]
    [SerializeField] int moveSpeed = 10;
    [SerializeField] float padding = 1f;
    [SerializeField] public int health = 200;

    [Header("Audio")]
    [SerializeField] AudioClip audioExplose;
    [Range(0.1f, 1f)][SerializeField] float volume = 1f;       

    [SerializeField] AudioClip audioLaser;
    [Range(0.1f, 1f)] [SerializeField] float volumeLaser = 1f;

    [Header("Projectile")]
    [SerializeField] GameObject laserPrefab;
    [SerializeField] float projectileSpeed = 10f;
    [SerializeField] float projectileFiringPeriod = 0.1f;

    Coroutine firingCorotine;

    float xMin, xMax, yMin, yMax;

    [SerializeField] GameObject playerExplosion;

    void Start()
    {
        SetUpMoveBoundarier();
        SetHealthScore();
    }

    private void SetHealthScore()
    {
        FindObjectOfType<GameSession>().SetHealth(health);
    }


    // Update is called once per frame
    void Update()
    {
        Move();
        Fire();
    }

    private void Fire()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            firingCorotine = StartCoroutine(FireContinuosly());
        }
        if (Input.GetButtonUp("Fire1"))
        {
            StopCoroutine(firingCorotine);
        }
    }

    IEnumerator FireContinuosly()
    {
        while (true)
        {
            GameObject laser = Instantiate(laserPrefab, transform.position, Quaternion.identity) as GameObject;
            laser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, projectileSpeed);

            AudioSource.PlayClipAtPoint(audioLaser, Camera.main.transform.position, volumeLaser);
            //Destroy(laser, 1f);
            yield return new WaitForSeconds(projectileFiringPeriod);
        }
    }

    private void Move()
    {
        var deltaX = Input.GetAxis("Horizontal") * moveSpeed * Time.deltaTime;
        var deltaY = Input.GetAxis("Vertical") * moveSpeed * Time.deltaTime;

        var newXPos = Mathf.Clamp(transform.position.x + deltaX, xMin, xMax);
        var newYPos = Mathf.Clamp(transform.position.y + deltaY, yMin, yMax);

        transform.position = new Vector2(newXPos, newYPos);


        //float horizontal = Input.GetAxis("Horizontal");
        //float vertical = Input.GetAxis("Vertical");
        //transform.Translate(new Vector2(horizontal, vertical) * moveSpeed * Time.deltaTime);
    }

    private void SetUpMoveBoundarier()
    {
        Camera gameCamera = Camera.main;
        xMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).x + padding;
        xMax = gameCamera.ViewportToWorldPoint(new Vector3(1, 0, 0)).x - padding;

        yMin = gameCamera.ViewportToWorldPoint(new Vector3(0, 0, 0)).y + padding;
        yMax = gameCamera.ViewportToWorldPoint(new Vector3(0, 1, 0)).y - padding;
    }

    private void OnTriggerEnter2D(Collider2D other)
    {        
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        

        if (!damageDealer) { return; };
        health -= damageDealer.GetDamage();
        FindObjectOfType<GameSession>().SetHealth(health);
        damageDealer.Hit();
        if (health <= 0)
        {
            DestroyPlayer();
        }

        if (other.gameObject.layer.Equals(9))
        {
            DestroyPlayer();
        }

    }

    private void DestroyPlayer()
    {
        AudioSource.PlayClipAtPoint(audioExplose, Camera.main.transform.position, volume);
        GameObject explosion = Instantiate(playerExplosion, transform.position, transform.rotation) as GameObject;
        FindObjectOfType<GameSession>().SetHealth(0);
        Destroy(gameObject);

        FindObjectOfType<Level>().LoadGameOver();
    }
}
