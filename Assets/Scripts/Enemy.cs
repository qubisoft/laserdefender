﻿using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] float health = 100;
    [SerializeField] float shotCounter;
    [SerializeField] float minTimeBetweenShots = 0.2f;
    [SerializeField] float maxTimeBetweenShots = 3f;

    [SerializeField] int scoreForKill = 100;

    [SerializeField] GameObject laser;
    [SerializeField] float laserSpeed = 10f;
    // Start is called before the first frame update

    [SerializeField] GameObject boomEfect;
    [SerializeField] float explosionDuration = 1f;

    [SerializeField] AudioClip audioExplose;
    [Range(0.1f, 1f)] [SerializeField] float volume = 1f;
    void Start()
    {        
        shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
    }

    // Update is called once per frame
    void Update()
    {        
        CountDownAndShoot();
    }

    private void CountDownAndShoot()
    {
        shotCounter -= Time.deltaTime;
        if (shotCounter <= 0f)
        {
            fire();
            shotCounter = Random.Range(minTimeBetweenShots, maxTimeBetweenShots);
        }
    }

    private void fire()
    {
        
        GameObject mylaser = Instantiate(laser, transform.position, Quaternion.identity) as GameObject;
        mylaser.GetComponent<Rigidbody2D>().velocity = new Vector2(0, -laserSpeed);

    }

    private void OnTriggerEnter2D(Collider2D other)
    {

       
        DamageDealer damageDealer = other.gameObject.GetComponent<DamageDealer>();
        if (!damageDealer) { return; };
        ProcessHit(damageDealer);
        

    }

    private void ProcessHit(DamageDealer damageDealer)
    {      
        health -= damageDealer.GetDamage();
        damageDealer.Hit();
        if (health <= 0)
        {
            Die();
        }
    }

    private void Die()
    {
        FindObjectOfType<GameSession>().addScore(scoreForKill);

        AudioSource.PlayClipAtPoint(audioExplose, Camera.main.transform.position, volume);        
        Destroy(gameObject);
        GameObject boom = Instantiate(boomEfect, transform.position, transform.rotation) as GameObject;
        Destroy(boom, explosionDuration);
    }
}
