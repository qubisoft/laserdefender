﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Level : MonoBehaviour
{
    [SerializeField] float delayInSecounds = 3f;
    // Start is called before the first frame update
    public void LoadGameOver()
    {
        StartCoroutine(LoadGameOverAfterSec());
        //SceneManager.LoadScene("GameOver");
    }
    public void LoadGame()
    {
        
        SceneManager.LoadScene(1);
        FindObjectOfType<GameSession>().ResetGame();
    }
    public void LoadStartMenu()
    {
        SceneManager.LoadScene(0);
    }
    public void QuitGame()
    {
        Application.Quit();
    }

    private IEnumerator LoadGameOverAfterSec()
    {
        while (true)
        {                        
            yield return new WaitForSeconds(delayInSecounds);
            SceneManager.LoadScene("GameOver");
        }
    }

}
